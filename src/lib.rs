use itertools::Itertools;

use std::fmt;
use std::ops::{Add, AddAssign, Index, IndexMut, Mul, SubAssign};

#[derive(Clone, Copy, PartialEq)]
pub enum PlayerId {
    Cross,
    Circle,
}

impl PlayerId {
    pub fn next(self) -> PlayerId {
        ((self as u8 + 1) % 2).into()
    }
}

impl From<u8> for PlayerId {
    fn from(n: u8) -> PlayerId {
        match n {
            0 => PlayerId::Cross,
            1 => PlayerId::Circle,
            _ => panic!(),
        }
    }
}

#[derive(Clone)]
pub enum GameResult {
    Draw,
    Winner {
        winner: PlayerId,
        winning_line: GridLine,
    },
}

pub struct Game {
    grid: Grid,
    current_player: PlayerId,
    n_vacant_cells: usize,
}

impl Game {
    pub fn new(grid: Grid) -> Self {
        let n_vacant_cells = grid.iter_raw().cloned().filter(Option::is_none).count();
        Self {
            grid,
            current_player: PlayerId::Cross,
            n_vacant_cells,
        }
    }

    pub fn make_move(&mut self, position: Position) -> Option<GameResult> {
        if self.grid[position].is_none() {
            self.n_vacant_cells -= 1;
        }
        self.grid[position] = Some(self.current_player);
        let w = self.grid.check_if_winning_move(position);
        if let Some(winning_line) = w {
            Some(GameResult::Winner {
                winner: self.current_player,
                winning_line,
            })
        } else {
            if self.n_vacant_cells == 0 {
                Some(GameResult::Draw)
            } else {
                self.current_player = self.current_player.next();
                None
            }
        }
    }

    pub fn into_grid(self) -> Grid {
        self.grid
    }

    pub fn get_grid(&self) -> &Grid {
        &self.grid
    }

    pub fn current_player(&self) -> PlayerId {
        self.current_player
    }
}

#[derive(Clone, Debug)]
pub enum GridLineDirection {
    Horizontal,
    Vertical,
    MainDiagonal,
    AntiDiagonal,
}

impl GridLineDirection {
    pub fn to_vector(&self) -> Position {
        use GridLineDirection::*;
        match self {
            Horizontal => Position(1, 0),
            Vertical => Position(0, 1),
            MainDiagonal => Position(1, 1),
            AntiDiagonal => Position(1, -1),
        }
    }
}

#[derive(Clone, Debug)]
pub struct GridLine {
    pub start: Position,
    pub direction: GridLineDirection,
    pub length: u8,
}

pub struct GridLineIter {
    current: Position,
    direction: GridLineDirection,
    remaining: u8,
}

impl IntoIterator for &GridLine {
    type Item = Position;
    type IntoIter = GridLineIter;

    fn into_iter(self) -> Self::IntoIter {
        GridLineIter {
            current: self.start,
            direction: self.direction.clone(),
            remaining: self.length,
        }
    }
}

impl Iterator for GridLineIter {
    type Item = Position;

    fn next(&mut self) -> Option<Self::Item> {
        if self.remaining == 0 {
            return None;
        }
        let current = self.current;
        self.current += &self.direction.to_vector();
        self.remaining -= 1;

        Some(current)
    }
}

#[derive(Clone)]
pub struct Grid {
    width: u8,
    height: u8,
    grid: Vec<Option<PlayerId>>,
}

#[derive(Clone, Copy, Debug)]
pub struct Position(pub i8, pub i8);

impl Add for Position {
    type Output = Position;

    fn add(self, other: Position) -> Self::Output {
        Position(self.0 + other.0, self.1 + other.1)
    }
}

impl<'a> AddAssign<&'a Position> for Position {
    fn add_assign(&mut self, other: &'a Position) {
        self.0 += other.0;
        self.1 += other.1;
    }
}

impl<'a> SubAssign<&'a Position> for Position {
    fn sub_assign(&mut self, other: &'a Position) {
        self.0 -= other.0;
        self.1 -= other.1;
    }
}

impl Mul<i8> for Position {
    type Output = Self;

    fn mul(self, rhs: i8) -> Self {
        Position(self.0 * rhs, self.1 * rhs)
    }
}

impl From<(u8, u8)> for Position {
    fn from(p: (u8, u8)) -> Self {
        Self(p.0 as i8, p.1 as i8)
    }
}

impl From<[u8; 2]> for Position {
    fn from(p: [u8; 2]) -> Self {
        Self(p[0] as i8, p[1] as i8)
    }
}

impl fmt::Display for Position {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Position({}, {})", self.0, self.1)
    }
}

impl Index<Position> for Grid {
    type Output = Option<PlayerId>;

    fn index(&self, position: Position) -> &Self::Output {
        let index = self.position_to_index(position);
        &self.grid[index]
    }
}

impl IndexMut<Position> for Grid {
    fn index_mut(&mut self, position: Position) -> &mut Self::Output {
        let index = self.position_to_index(position);
        &mut self.grid[index]
    }
}

impl Grid {
    pub fn new(width: u8, height: u8) -> Self {
        Grid {
            width: width,
            height: height,
            grid: vec![None; width as usize * height as usize],
        }
    }

    pub fn iter_occupied<'a>(&'a self) -> impl Iterator<Item = (Position, PlayerId)> + 'a {
        self.iter()
            .flat_map(|(pos, player)| player.map(|x| (pos, x)))
    }

    pub fn iter<'a>(&'a self) -> impl Iterator<Item = (Position, Option<PlayerId>)> + 'a {
        (0..self.width)
            .cartesian_product(0..self.height)
            .map(Into::into)
            .zip(self.grid.iter().cloned())
    }

    pub fn iter_raw(&self) -> impl Iterator<Item = &Option<PlayerId>> {
        self.grid.iter()
    }

    pub fn width(&self) -> u8 {
        self.width
    }

    pub fn height(&self) -> u8 {
        self.height
    }

    pub fn position_is_in_bounds(&self, position: Position) -> bool {
        position.0 < self.width as i8 && position.1 < self.height as i8
    }

    fn position_to_index(&self, position: Position) -> usize {
        position.0 as usize * self.height as usize + position.1 as usize
    }

    pub fn place_mark(&mut self, player: PlayerId, position: Position) {
        self[position] = Some(player);
    }

    fn find_line_end_and_length(
        &self,
        player: PlayerId,
        mut position: Position,
        direction_vector: Position,
    ) -> (Position, u8) {
        let mut length = 0;
        loop {
            let next_position = position + direction_vector;
            if next_position.0 < 0
                || next_position.0 >= self.width as i8
                || next_position.1 < 0
                || next_position.1 >= self.height as i8
            {
                break;
            }

            match self[next_position] {
                Some(ref mark_owner) if mark_owner == &player => length += 1,
                _ => break,
            }

            position = next_position;
        }

        (position, length)
    }

    pub fn check_if_winning_move(&self, start_position: Position) -> Option<GridLine> {
        let player = match self[start_position] {
            Some(mark_owner) => mark_owner,
            None => return None,
        };

        use GridLineDirection::*;
        let directions = [Horizontal, Vertical, MainDiagonal, AntiDiagonal];
        let goal_length = std::cmp::min(self.width, self.height);

        for direction in &directions {
            let mut length = 1;

            let (line_start, l) =
                self.find_line_end_and_length(player, start_position, direction.to_vector() * -1);
            length += l;
            length += self
                .find_line_end_and_length(player, start_position, direction.to_vector())
                .1;

            if length >= goal_length {
                return Some(GridLine {
                    start: line_start,
                    direction: direction.clone(),
                    length,
                });
            }
        }

        None
    }
}
